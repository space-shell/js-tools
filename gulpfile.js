const pump = require('pump')
const gulp = require('gulp')
const babel = require('gulp-babel')
const uglify = require('gulp-uglify')
const rename = require("gulp-rename")
const jsdoc = require('gulp-jsdoc3')

const config = require('./jsdoc.json')

const sauce = [
  'src/**/*.js',
  '!src/**/*.test.js',
  '!src/**/*.conf.js',
  '!src/**/*.mocks.js',
  '!src/**/*.min.js'
]

gulp.task('build', (cb) => {
  pump([
    gulp.src(sauce),
    babel({ presets: ['es2015'] }),
    uglify(),
    rename({ suffix: ".min" }),
    gulp.dest(f => {
      return f.base
    })
  ], cb);
})

gulp.task('doc', cb => {
  pump([
    gulp.src(['README.md', 'src/**/*.js'], {read: false}),
    jsdoc(config, cb)
  ], cb);
});

gulp.task('default', ['build', 'doc'])