const pathArray = (paths, root) => {
	const bs = function(bs){
		return bs.charAt(1) === `/` ? `` : `/` }
	const string = function(){
		return Object.keys(arguments).map(s => arguments[s].replace(/\\/g,"/"))
			.join(``) }
	for(let path in paths){
		if(typeof paths[path] === `string`) {
			paths[path] = paths[path] === `` ? paths[path] : string(root, bs(paths[path]), paths[path])
		} else {
			paths[path] = paths[path].map(p => {
				return p === `` ? p : string(root, bs(p), p) })
		}
	}
	return paths
}

const makeArray = (str) => {
  return [...Array].concat(str).slice(1)
}
