import UrlTools from './url-tools'

const j = JSON.parse(`{ "url": "https://49itpnp2eh.execute-api.eu-west-1.amazonaws.com/dev/granted-permits/default", "params": [ { "startIssueDate": "20170929" }, { "endIssueDate": "20171009" }, { "notifiableOrganisationName": "BARNET" }, { "noticePrefix": "MX037" } ] }`)
const p = `https://49itpnp2eh.execute-api.eu-west-1.amazonaws.com/dev/granted-permits/default?startIssueDate=20170929&endIssueDate=20171009&notifiableOrganisationName=BARNET&noticePrefix=MX037`

test(`Check Function Params Query`, () => {
  expect(UrlTools().paramsQuery(j.url, j.params)).toEqual(p)
})

test(`Check Function Params Json`, () => {
  expect(UrlTools().paramsJson(p)).toEqual(j)
})