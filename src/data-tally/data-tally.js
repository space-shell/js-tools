const DataTally = (arr, query) => {
  let expo = {}
  expo.tally = () => {
    let t = {}
    arr.forEach(x => {
      for(let y in x){
        if(y.indexOf(query) > -1){
          t[x[y]] = (t[x[y]] || 0) + 1 }}})
    return t
  }
  expo.total = (tally = expo.tally()) => {
    return Object.keys(tally).reduce((tot, tal) => {
      return tot += tally[tal]
    },0)
  }

  return expo
}

export default DataTally
let data = require('./data-tally.mocks.json')
let test = DataTally(data, `noticePrefix`)
//Test Data
console.log(test.tally())
console.log(test.total())
