import DataTally from './data-tally'

const mocks = require('./data-tally.mocks.json')

let func = DataTally(mocks, `noticePrefix`)

console.log(func.tally())
console.log(func.total())

test(`Tally data against query`, () => {
  expect(DataTally(mocks, `noticePrefix`).tally().toEqual('{ MX044: 182, MX043: 3, MX047: 1, MX045: 4, MX037: 1, MX049: 1 }'))
})

test(`Total tally data`, () => {
  expect(DataTally(mocks, `noticePrefix`).total().toEqual(192))
})

let data = require('./data-tally.mocks.json')
