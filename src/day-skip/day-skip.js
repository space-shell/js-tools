/**
* @author James Nicholls
* @module DaySkip
* @desc Handles Dates and Date arrays that primarily omit weekends.
*/

/**
 * @constructor module:DaySkip
 * @param {Int} days - The number of days excluding weekends
 * @param {Bool} rev - Check forwards or backwards in time
 * @param {Date} start - The start date to begin the weekend checks
 */
const DaySkip = (days = 7, rev = false, start = new Date()) => {
  const NOW = new Date()
  const dir = rev ? -1 : 1
  //days = rev ? days + 1 : days

  let expo = {}
  /**
   * @function skips
   * @param {Int} count
   * @param {Int} day
   * @desc Calculates the weekend offset from the start date
   * @returns {Array} Returns an array of corrisponding days skipped from the start date
   */
  expo.skips = (count = days, day = start.getDay()) => {
    day--
    return [...Array(days)].map((u, i) => {
      let x = i + day
      let floater = rev ? Math.floor(-x/5) : Math.floor(x/5)
      return floater * 2 * dir
    })
  }
  /**
   * @function skipsTotal
   * @param {Int} count
   * @param {Int} day
   * @desc Calculates the total number of weekends from the start date
   * @returns {Int} Returns the number of weekends skipped
   */
  expo.skipsTotal = (count = days, day = start.getDay()) => {
    let skipsTotal = expo.skips(count, day)
    return skipsTotal[count-1]
  }
  /**
   * @function skipsList
   * @param {Int} count
   * @param {Int} day
   * @param {Date} date
   * @desc Calculates the total number of weekends from the start date
   * @returns {(Array|Date)} Returns the number of weekends skipped in JavaScript Date Format
   */
  expo.skipsList = (count = days, day = start.getDay(), date = start) => {
    let skipsList = expo.skips(count, day)

    return skipsList.map((d, i) => {
      let nextDay = new Date()
      nextDay.setTime(start.getTime())
      nextDay.setDate(nextDay.getDate()+i+d)
      return nextDay
    })
  }
  /**
   * @function daysCount
   * @param {Date} begin
   * @param {Date} end
   * @desc Calculates number of days between two dates
   * @returns {Int} Returns the number of days between two dates
   */
  expo.daysCount = (begin = start, end = NOW) => {
    return Math.floor((end-begin)/(1000*60*60*24))
  }
  /**
   * @function sap
   * @param {Date} date
   * @desc Converts a JavaScript date into the SAP date format
   * @returns {String} Returns the SAP date format
   */
  expo.sap = (date = new Date()) => {
    let m = (date.getMonth()+1).toString()
    m = m.length === 1 ? `0${m}` : m
    let d = date.getDate().toString()
    d = d.length === 1 ? `0${d}` : d 
    return date.getFullYear().toString() + m + d
  }
  /**
   * @function skipsListSap
   * @param {(Array|Date)} dates
   * @desc Converts an array of JavaScript dates into the SAP date format
   * @returns {String} Returns the array of SAP date formats
   */
  expo.skipsListSap = (dates = expo.skipsList()) => {
    return dates.map(d => expo.sap(d))
  }
  /**
   * @function monthRange
   * @param {Int} month
   * @desc Calculates the first and last date of the month
   * @desc { firstDay: Date, lastDay: Date }
   * @returns {Object} Returns the first and last date of the month in JavaScript Date format
   */
  expo.monthRange = (month = NOW.getMonth()) => {
    let date = new Date(NOW.getFullYear(), month, NOW.getDate())
    var y = date.getFullYear(), m = date.getMonth()
    return { firstDay: new Date(y, m, 1), lastDay: new Date(y, m + 1, 0) }
  }
  /**
   * @function monthRangeSap
   * @param {Int} month
   * @desc Calculates the first and last date of the month
   * @desc { firstDay: String, lastDay: String }
   * @returns {Object} Returns the first and last date of the month in the SAP Date format
   */
  expo.monthRangeSap = (month = NOW.getMonth()) => {
    let months = expo.monthRange(month)
    return { firstDay: expo.sap(months.firstDay), lastDay: expo.sap(months.lastDay) }
  }
  /**
   * @function monthRangeOffset
   * @param {Int} month
   * @desc Calculates the number of days from now until the first and last date of the month
   * @desc { start: Int, end: Int, total: Int }
   * @returns {Object} Returns the number of days from now until the first and last date of the month
   */
  expo.monthRangeOffset = (month = NOW.getMonth()) => {
    let months = expo.monthRange(month)
    return { start: expo.daysCount(months.firstDay, NOW), end: expo.daysCount(NOW, months.lastDay), total: expo.daysCount(months.firstDay,months.lastDay) }
  }
  /**
   * @function sapJs
   * @param {String} sap
   * @desc Converts a SAP date into a JavaScript Date object
   * @returns {Date} Returns a JavaScript Date object
   */
  expo.sapJs = (sap) => {
    let dateSplit = [
      parseInt(sap.substring(0, 4), 10),
      (parseInt(sap.substring(4, 6), 10)-1),
      parseInt(sap.substring(6, 8), 10)
    ]
    return new Date(...dateSplit)
  }
  /**
   * @function sapJsList
   * @param {(Array|String)} list
   * @desc Converts a list of SAP dates into a JavaScript Date objects
   * @returns {(Array|Date)} Returns a list of SAP dates into a JavaScript Date objects
   */
  expo.sapJsList = (list) => {
    return list.map(l => expo.sapJs(l))
  }

  return expo
}

export default DaySkip

/*
//Test Data
const test = DaySkip(7, false, new Date(2017, 9, 9))
//Test 
console.log(test.skips())
console.log(test.skipsTotal())
console.log(test.skipsList())
console.log(test.daysCount())
console.log(test.sap())
console.log(test.skipsListSap())
console.log(test.monthRange())
console.log(test.monthRangeSap())
console.log(test.monthRangeOffset())
console.log(test.sapJs(DaySkip().sap()))
console.log(test.sapJsList([DaySkip().sap(), DaySkip().sap()]))
 */